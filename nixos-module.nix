self:
{ pkgs, config, lib, ... }:

let
  inherit (self.packages.${pkgs.stdenv.system}) ticker-update ticker-serve;
  inherit (lib) mkOption types;
  cfg = config.services.ticker;
  defaultTickerConfig = {
    db_url = "postgres:///ticker";
    calendars = {};
    weekdays = [
      "Montag" "Dienstag" "Mittwoch" "Donnerstag"
      "Freitag" "Sonnabend" "Sonntag"
    ];
    months = [
      "Januar" "Februar" "März" "April"
      "Mai" "Juni" "Juli" "August"
      "September" "Oktober" "November" "Dezember"
    ];
    upcoming_days = 28;
    passed_days = 7;
    http_bind = "0.0.0.0:8400";
  };
  tickerConfig = defaultTickerConfig // cfg.config;
  configFile = pkgs.writeText "config.yaml" (lib.generators.toYAML {} tickerConfig);
  workDir = pkgs.runCommandLocal "ticker-env" {} ''
    mkdir $out
    ln -s ${ticker-serve}/shared/ticker-serve/static $out/
    ln -s ${configFile} $out/config.yaml
  '';
in
{
  options.services.ticker = {
    stateDir = mkOption {
      type = types.str;
      default = "/var/lib/ticker";
      description = ''
        Directory where ticker files will be placed by default.
      '';
    };
    user = mkOption {
      type = types.str;
      default = "ticker";
      description = "User account under which ticker runs.";
    };
    group = mkOption {
      type = types.str;
      default = "ticker";
      description = "Group account under which ticker runs.";
    };
    config = mkOption {
      type = types.attrs;
      default = defaultTickerConfig;
    };
    updateInterval = mkOption {
      type = types.str;
      default = "hourly";
      description = "Timer interval";
    };
  };

  config = {
    users.users.${cfg.user} = {
      inherit (cfg) group;
      isSystemUser = true;
    };
    users.groups.${cfg.group} = {};

    services.postgresql = {
      enable = true;
      ensureDatabases = [ "ticker" ];
    };

    systemd.services.ticker-setup = {
      wantedBy = [ "multi-user.target" ];
      after = [ "postgresql.service" ];
      preStart = let
        pgsu = config.services.postgresql.superUser;
        psql = config.services.postgresql.package;
      in ''
        mkdir -p ${cfg.stateDir}
        chown ${cfg.user}:${cfg.group} -R ${cfg.stateDir}
        if ! test -e "${cfg.stateDir}/db-created"; then
          ${lib.getExe pkgs.sudo} -u ${pgsu} ${psql}/bin/createuser --no-superuser --no-createdb --no-createrole ${cfg.user}
          ${lib.getExe pkgs.sudo} -u ${cfg.user} ${psql}/bin/psql -f ${ticker-serve}/shared/libticker/schema.sql ticker
          touch "${cfg.stateDir}/db-created"
        fi
      '';
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.coreutils}/bin/test -f ${cfg.stateDir}/db-created";
      };
    };

    systemd.services.ticker-update = {
      wantedBy = [ "multi-user.target" ];
      after = [ "postgresql.service" "ticker-setup.service" ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${ticker-update}/bin/ticker-update";
        WorkingDirectory = "${workDir}";
        User = cfg.user;
        Group = cfg.group;
      };
    };
    systemd.timers.ticker-update = {
      wantedBy = [ "multi-user.target" ];
      partOf = [ "ticker-update.service" ];
      timerConfig = {
        OnCalendar = cfg.updateInterval;
      };
    };

    systemd.services.ticker-serve = {
      wantedBy = [ "multi-user.target" ];
      after = [ "postgresql.service" "ticker-setup.service" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${ticker-serve}/bin/ticker-serve";
        WorkingDirectory = "${workDir}";
        User = cfg.user;
        Group = cfg.group;
      };
    };
  };
}
