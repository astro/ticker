use std::sync::{Arc, Mutex};
use axum::{
    Router,
    routing::get, Extension,
};
use tower_http::services::ServeDir;
use diesel::{Connection, pg::PgConnection};

use libticker::config::Config;
mod index;
mod export;

#[derive(Clone)]
pub struct AppState {
    pub db: Arc<Mutex<PgConnection>>,
    pub config: Arc<Config>,
}

#[tokio::main]
async fn main() {
    let config = Config::read_yaml_file("config.yaml");
    let listener = tokio::net::TcpListener::bind(&config.http_bind)
        .await
        .unwrap();
    let db = PgConnection::establish(&config.db_url)
        .expect("DB");

    let state = AppState {
        db: Arc::new(Mutex::new(db)),
        config: Arc::new(config),
    };
    let app = Router::new()
        .route("/", get(index::index))
        .route("/export.ics", get(export::ics))
        .layer(Extension(state))
        .nest_service("/static", ServeDir::new("static"));
    axum::serve(listener, app.into_make_service())
        .await
        .unwrap();
}
