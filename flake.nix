{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
    fenix.url = "github:nix-community/fenix";
    fenix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, fenix, naersk }:
    utils.lib.eachSystem (with utils.lib.system; [ x86_64-linux aarch64-linux ]) (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      rust = fenix.packages.${system}.stable.withComponents [
        "cargo"
        "rustc"
        "rust-src"  # just for rust-analyzer
        "clippy"
      ];

      # Override the version used in naersk
      naersk-lib = naersk.lib."${system}".override {
        cargo = rust;
        rustc = rust;
      };

      buildRustPackage = args: naersk-lib.buildPackage ({
        name = args.pname;
        version =
          let
            inherit (self) lastModifiedDate;
            year = builtins.substring 0 4 lastModifiedDate;
            month = builtins.substring 4 2 lastModifiedDate;
            day = builtins.substring 6 2 lastModifiedDate;
          in "0.1.0-${year}-${month}-${day}";
        src = ./.;
        targets = [ args.pname ];
        cargoBuildOptions = opts: opts ++ [ "-p" args.pname ];
        nativeBuildInputs = with pkgs; [ pkg-config ];
        buildInputs = with pkgs; [ openssl postgresql.lib ];
        meta.mainProgram = args.pname;
      } // args);
    in rec {
      # `nix build`
      packages.ticker-update = buildRustPackage {
        pname = "ticker-update";
      };
      packages.ticker-serve = buildRustPackage {
        pname = "ticker-serve";
        overrideMain = x: {
          installPhase = ''
            ${x.installPhase}

            mkdir -p $out/shared/libticker $out/shared/ticker-serve
            cp -ar ticker-serve/static $out/shared/ticker-serve/
            cp -ar schema.sql $out/shared/libticker/
          '';
        };
      };

      checks = packages;

      # `nix develop`
      devShells.default = pkgs.mkShell {
        nativeBuildInputs = [
          fenix.packages.${system}.rust-analyzer
        ] ++ (with packages.ticker-serve;
          nativeBuildInputs ++ buildInputs
        );
      };
    }) // rec {
      overlays.default = final: prev: {
        inherit (self.packages.${prev.system}) ticker-update ticker-serve;
      };

      nixosModules = rec {
        ticker = import ./nixos-module.nix self;
        default = ticker;
      };
    };
}
