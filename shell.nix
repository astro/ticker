{ pkgs ? import <nixpkgs> {} }:

let
  default = import ./default.nix { inherit pkgs; };
in

pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs =
    with pkgs;
    with rustPlatform.rust;
    [ rustc cargo rust-analyzer pkg-config ] ++
    default.ticker-update.buildInputs ++
    default.ticker-serve.buildInputs;

  shellHook = ''
    echo "Run 'cargo build --release'"
  '';
}
