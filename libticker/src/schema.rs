table! {
    calendars  {
        id -> VarChar,
        url -> VarChar,

        last_fetch -> Nullable<Timestamp>,
        last_success -> Nullable<Timestamp>,
        error_message -> Nullable<VarChar>,

        etag -> Nullable<VarChar>,
        last_modified -> Nullable<VarChar>,
    }
}

table! {
    events (calendar, id) {
        calendar -> VarChar,
        id -> VarChar,

        dtstart -> Timestamp,
        dtend -> Nullable<Timestamp>,
        summary -> VarChar,
        location -> Nullable<VarChar>,
        url -> Nullable<VarChar>,

        recurrence -> Bool,
    }
}
