use std::fs::read_to_string;
use std::collections::BTreeMap;
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct CalendarDefaults {
    pub location: Option<String>,
    pub url: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct CalendarOptions {
    pub url: String,
    pub color: String,
    pub defaults: Option<CalendarDefaults>,
    pub overrides: Option<CalendarDefaults>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Config {
    pub db_url: String,
    pub http_bind: String,
    pub calendars: BTreeMap<String, CalendarOptions>,
    pub weekdays: Vec<String>,
    pub months: Vec<String>,
    /// show how much into the future
    pub upcoming_days: u32,
    /// show events only if they started no more than this many days ago
    pub passed_days: u32,
}

impl Config {
    pub fn read_yaml_file(path: &str) -> Self {
        let config_file = read_to_string(path)
            .expect(path);
        serde_yaml::from_str(&config_file)
            .expect("config")
    }
}
