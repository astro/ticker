#[macro_use]
extern crate diesel;

pub mod config;
pub mod ics;
pub mod model;
pub mod schema;
