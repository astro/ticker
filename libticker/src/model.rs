use chrono::NaiveDateTime;
use super::schema::{calendars, events};

#[derive(Debug, Queryable)]
pub struct Calendar {
    pub id: String,
    pub url: String,

    pub last_fetch: Option<NaiveDateTime>,
    pub last_success: Option<NaiveDateTime>,
    pub error_message: Option<String>,

    pub etag: Option<String>,
    pub last_modified: Option<String>,
}

#[derive(Debug, Insertable)]
#[table_name = "calendars"]
pub struct NewCalendar {
    pub id: String,
    pub url: String,

    pub last_fetch: Option<NaiveDateTime>,
}

#[derive(Debug, Queryable, Insertable)]
pub struct Event {
    pub calendar: String,
    pub id: String,

    pub dtstart: NaiveDateTime,
    pub dtend: Option<NaiveDateTime>,
    pub summary: String,
    pub location: Option<String>,
    pub url: Option<String>,

    pub recurrence: bool,
}
