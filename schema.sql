CREATE TABLE calendars (
       id TEXT PRIMARY KEY,
       url TEXT NOT NULL,

       last_fetch TIMESTAMP,
       last_success TIMESTAMP,
       error_message TEXT,

       etag TEXT,
       last_modified TEXT
);

CREATE TABLE events (
       calendar TEXT NOT NULL,
       id TEXT NOT NULL,
       PRIMARY KEY (calendar, id),

       dtstart TIMESTAMP NOT NULL,
       dtend TIMESTAMP,

       summary TEXT NOT NULL,
       location TEXT,
       url TEXT,
       recurrence BOOL
);
CREATE index events_dtstart_dtend ON events (dtstart, dtend);
